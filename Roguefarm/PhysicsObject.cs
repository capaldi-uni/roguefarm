﻿using System;
using SFML.System;
using SFML.Graphics;

namespace Roguefarm
{
    class PhysicsObject
    {
        //Collider parameters
        protected Vector2f _center;
        protected float _radius;

        //Physical parameters
        protected float _mass;
        protected Vector2f _force = MathV.Zero;
        protected Vector2f _velocity = MathV.Zero;
        protected float _terminal_velocity = 10;

        public Vector2f Position { get => _center; set => _center = value; }
        public float Radius { get => _radius; set { if (value > 0) _radius = value; } }

        public float Mass { get => _mass; set { if (value > 0) _mass = value; } }
        public float Speed { get => MathV.Length(_velocity); }
        public Vector2f Velocity { get => _velocity; }
        public float MaxSpeed { get => _terminal_velocity; set { if (value > 0) _terminal_velocity = value; } }


        public PhysicsObject(Vector2f c, float r, float m) { _center = c; _radius = r; _mass = m; }

        //Collision checks
        public bool Collide(PhysicsObject other)
        {
            float ox = other._center.X;
            float oy = other._center.Y;

            float distance = MathF.Sqrt(MathF.Pow(ox - _center.X, 2) + MathF.Pow(oy - _center.Y, 2));
            return distance <= _radius + other._radius;
        }
        public bool Collide(Vector2f A, Vector2f B)
        {
            A -= _center;
            B -= _center;

            float a = MathF.Pow(B.X - A.X, 2) + MathF.Pow(B.Y - A.Y, 2);
            float b = 2 * (A.X * (B.X - A.X) + A.Y * (B.Y - A.Y));
            float c = MathF.Pow(A.X, 2) + MathF.Pow(A.Y, 2) - MathF.Pow(_radius, 2);

            float disc = MathF.Pow(b, 2) - 4 * a * c;
            
            if (disc <= 0) return false;

            disc = MathF.Sqrt(disc);
            float t1 = (-b - disc) / (2 * a);
            float t2 = (-b + disc) / (2 * a);

            if ((0 < t1 && t1 < 1) || (0 < t2 && t2 < 1)) return true;

            return false;

        }

        //Physics methdos
        public void AddForce(Vector2f force)
        {
            _force += force;
        }
        public void AddImpulse(Vector2f impulse)
        {
            _velocity += impulse / _mass;
        }
        public void AddVelocity(Vector2f velocity)
        {
            _velocity += velocity;
        }

        public void PlaceAt(Vector2f pos)
        {
            _velocity = MathV.Zero;
            _force = MathV.Zero;
            Position = pos;
        }
        public void ForceStop()
        {
            _velocity = MathV.Zero;
        }

        public void UpdatePhysics(float delta)
        {
            //friction  -- rewrite using forces
            float newvel = MathV.Length(_velocity) - _mass * Physics.Mu;
            _velocity = MathV.Normalize(_velocity) * (newvel > 0 ? newvel : 0);

            //force application
            Vector2f acc = (_force / _mass) * Physics.A;
            _force = MathV.Zero;

            //acceleration
            _velocity += acc * delta;

            //velocity termination
            if (MathV.Length(_velocity) > _terminal_velocity)
                _velocity = MathV.Normalize(_velocity) * _terminal_velocity;

            Position += _velocity * delta;
        }

        //virual hooks to override
        public virtual void Update(float delta) { }

        public virtual void OnCollision(PhysicsObject other)
        {
            AddForce(MathV.Normalize(Position - other.Position) * Physics.N);
        }

        public virtual void OnCollision(Wall wall)
        {
            //Calculate line equation
            float A = wall.A.Y - wall.B.Y;
            float B = wall.B.X - wall.A.X;
            float C = wall.A.X * wall.B.Y - wall.B.X * wall.A.Y;

            //Find semiplane the object is on
            int sign = MathV.Sign(Position.X * A + Position.Y * B + C);
            //Create normal vector from line into found semiplane
            Vector2f normal = MathV.Normalize(new Vector2f(A, B)) * sign;

            AddForce(normal * 2 * Physics.N);   //push back a bit
                                            //Set to 0 component of object's velocity facing against the wall using projection of objects velocity on inverse normal vector
            AddVelocity(-MathV.Project(Velocity, -normal));
        }
    }
}
