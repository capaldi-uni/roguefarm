﻿using System;
using SFML.System;
using SFML.Graphics;

namespace Roguefarm
{
    class Player : Character
    {
        Texture texture = new Texture("Sprites/Selected/character.png");

        public static float basicRad = 50;
        public static float basicThick = 5;
        public static Color basicColor = new Color(20, 100, 120);
        public static float basicMass = 1.2f;
        public static float basicHP = 100;
        public static float basicDMG = 20f;
        public static float basicDEF = 0;
        public static float basicAcc = 1;
        public static float basicShotDelay = 30;
        
        public bool A = false;
        public bool D = false;
        public bool S = false;
        public bool W = false;

        public bool LMB = false;
        public Vector2f MP = MathV.Zero;
        private float shotDelay = basicShotDelay;
        private float shotCounter = 0;

        public Player(Vector2f pos) : 
            base(pos, basicMass, new HitboxParam() { rad = basicRad, color = basicColor, thickness = basicThick }, 
                basicHP, basicDMG, basicDEF, basicAcc) 
        {

            Sprite = new Sprite(texture) { Scale = new Vector2f(.5f, .5f) };
            DamageCooldown = 30;
        }

        //Shoot projectile at cursor position
        private void Shoot(Vector2f at)
        {
            //define direction
            Vector2f shootDirection = MathV.Normalize(at - Position);
            Vector2f shootPostition = Position + shootDirection * 25;

            //Spawn projectile
            PlayerProjectile proj = new PlayerProjectile(shootPostition, shootDirection, this);
            Program.MarkForAdd(proj);
        }

        public override void Update(float delta)
        {
            Vector2f direction = new Vector2f();

            //Detect input
            if (A) direction.X = -1;
            if (D) direction.X = +1;
            if (W) direction.Y = -1;
            if (S) direction.Y = +1;

            if (!A && !D) direction.X = 0;
            if (!W && !S) direction.Y = 0;

            //Movement according to input
            direction = MathV.Normalize(direction);
            MoveIn(direction);

            //Shot delay
            if (shotCounter > 0)
                shotCounter -= delta;

            //Shoot if possible
            if (LMB && shotCounter <= 0)
            {
                Shoot(MP);
                shotCounter = shotDelay;
            }

            base.Update(delta);
        }
    }
}
