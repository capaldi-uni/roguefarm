﻿using System;
using System.Collections.Generic;
using SFML.System;
using SFML.Window;
using SFML.Graphics;

namespace Roguefarm
{
    class Program
    {
        #region System
        static RenderWindow window;
        static Clock clock;
        static Font JBM = new Font("Fonts\\JetBrainsMono-Medium_0.ttf");
        #endregion

        #region Values
        //Screen parameters
        static float w;     //Width
        static float h;     //Height

        static float d;     //Border width -- this probably will be removed as soon as textures come
        static float dw = 228;
        #endregion

        #region Objects
        static Map map;
        static Room room { get => map.CurrentRoom; }    //current room reference
        static Player player = null;    //player object

        static List<SpriteObject> overlayObjects = new List<SpriteObject>();  //Sprite objects to be drawn after everything else
        static List<Drawable> overlays = new List<Drawable>();
        static Text playerHpInfo;
        #endregion

        #region debug
        static bool debug = false;      //Is debug mode one

        static List<Vertex[]> lines = new List<Vertex[]>(); //Vectors representing walls and objects' speeds
        
        static int fps = 60;    //Displayed framerate
        #endregion

        static void Main(string[] args)
        {
            if(false) //testing section. To execute some code instead of the game
            {
                Console.WriteLine(MathV.Project(new Vector2f(2, 0), new Vector2f(3, 3)));

                return;
            }

            #region System Setup
            clock = new Clock();
            w = 1366;
            h = 768;

            window = new RenderWindow(new VideoMode((uint)w, (uint)h), "Roguefarm");
            window.SetFramerateLimit(60);
            
            //Event handlers
            window.Closed += OnClose;
            window.KeyPressed += OnKeyPressed;
            window.KeyReleased += OnKeyReleased;
            window.MouseButtonPressed += OnMousePressed;
            window.MouseButtonReleased += OnMouseReleased;
            window.MouseMoved += OnMouseMoved;
            #endregion


            #region Scene Setup
            //This probably will be gone after textures' implementation
            Color bg = new Color(200, 120, 20);

            d = 100;
            
            //That's it. No menu right now
            NewMap();

            #region debug
            #endregion

            #endregion


            while (window.IsOpen)   //Main loop
            {
                #region System
                window.DispatchEvents();
                float delta = clock.Restart().AsSeconds() * 60; //Scaled by 60 to be somewhat reasonable
                fps = (int)(60 / MathF.Round(delta, 1));    //Framerate tracking
                #endregion


                #region Updating
                if (delta > 5)
                    delta = 5;

                room.Update(delta);
                Physics.Update(delta);

                if (player != null)
                    playerHpInfo.DisplayedString = string.Format("{0}/{1}", (int)player.Health, (int)Player.basicHP);
                else
                    playerHpInfo.DisplayedString = string.Format("0/{0}", (int)Player.basicHP);
                #endregion


                #region Display
                window.Clear(bg);

                room.Draw(window, debug);
                DrawOverlays();

                #region debug
                if (debug)
                {
                    lines.Clear();

                    foreach(Wall wall in room.Walls)
                    {
                        Color c;
                        if (wall.Solid)
                            c = new Color(255, 255, 255);
                        else
                            c = new Color(100, 100, 100);
                        lines.Add(new Vertex[] { new Vertex(wall.A, c), new Vertex(wall.B, c) });

                        Vector2f middle = MathV.Middle(wall.A, wall.B);
                        Vector2f rotated = MathV.Rotate(wall.B - wall.A, -90) / 10;
                        lines.Add(new Vertex[] { new Vertex(middle, c),
                                             new Vertex(middle + rotated, c) });
                    }

                    foreach (SpriteObject s in room.Objects)
                        lines.Add(new Vertex[] { new Vertex(s.Position), new Vertex(s.Position + 5 * s.Velocity) });

                    if (player != null)
                        lines.Add(new Vertex[] { new Vertex(player.Position), new Vertex(player.Position + 5 * player.Velocity) });

                    foreach (Vertex[] line in lines)
                        window.Draw(line, 0, 2, PrimitiveType.LineStrip);

                    ShowDebugInfo();
                }
                #endregion

                window.Display();
                #endregion
            }
        }

        #region Event Handlers
        static void OnClose(object sender, EventArgs e)
        {
            ((Window)sender).Close();
        }
        static void OnKeyPressed(object sender, EventArgs e)
        {
            switch (((KeyEventArgs)e).Code)
            {
                case Keyboard.Key.A:
                    if (player != null)
                        player.A = true;
                    break;
                case Keyboard.Key.D:
                    if (player != null)
                        player.D = true;
                    break;
                case Keyboard.Key.W:
                    if (player != null)
                        player.W = true;
                    break;
                case Keyboard.Key.S:
                    if (player != null)
                        player.S = true;
                    break;

                case Keyboard.Key.R:
                    Restart();
                    break;

                case Keyboard.Key.Tilde:
                    debug = !debug;
                    break;
            }

            #region debug
            if (debug)
                switch (((KeyEventArgs)e).Code)
                {
                    case Keyboard.Key.Numpad4:
                        room.ToggleDoor(0);
                        break;
                    case Keyboard.Key.Numpad2:
                        room.ToggleDoor(1);
                        break;
                    case Keyboard.Key.Numpad6:
                        room.ToggleDoor(2);
                        break;
                    case Keyboard.Key.Numpad8:
                        room.ToggleDoor(3);
                        break;
                    case Keyboard.Key.L:
                        KillEnemies();
                        break;
                }
            #endregion
        }
        static void OnKeyReleased(object sender, EventArgs e)
        {
            switch (((KeyEventArgs)e).Code)
            {
                case Keyboard.Key.A:
                    if (player != null)
                        player.A = false;
                    break;
                case Keyboard.Key.D:
                    if (player != null)
                        player.D = false;
                    break;
                case Keyboard.Key.W:
                    if (player != null)
                        player.W = false;
                    break;
                case Keyboard.Key.S:
                    if (player != null)
                        player.S = false;
                    break;
            }
        }
        static void OnMousePressed(object sender, EventArgs e)
        {
            MouseButtonEventArgs args = (e as MouseButtonEventArgs);

            if (args.Button == Mouse.Button.Left)
                if (player != null)
                    player.LMB = true;

            #region debug
            if (debug)
            {
                if (args.Button == Mouse.Button.XButton2)
                    SpawnEnemy(new Vector2f(args.X, args.Y));
                else if (args.Button == Mouse.Button.XButton1)
                    player.Position = new Vector2f(args.X, args.Y);
            }
            #endregion

        }
        static void OnMouseReleased(object sender, EventArgs e)
        {
            MouseButtonEventArgs args = (e as MouseButtonEventArgs);

            if (args.Button == Mouse.Button.Left)
                if (player != null)
                    player.LMB = false;
        }
        static void OnMouseMoved(object sender, EventArgs e)
        {
            MouseMoveEventArgs args = (e as MouseMoveEventArgs);

            if (player != null)
                player.MP = new Vector2f(args.X, args.Y);
        }
        #endregion

        #region Object Management
        private static void RegisterObject(SpriteObject c)
        {
            room.MarkForAdd(c);
        }
        public static void MarkForAdd(SpriteObject c)
        {
            room.MarkForAdd(c);
        }
        public static void MarkForDelete(SpriteObject c)
        {
            room.MarkForDelete(c);
        }
        #endregion


        #region Game
        private static void Restart()
        {
            NewMap();
            overlays.Clear();
        }
        private static void NewMap()
        {
            map = new Map(3, new Room.Parameters(w, h, d, dw, null));
            SpawnPlayer();
        }
        private static void NewMap(int seed)
        {
            map = new Map(3, new Room.Parameters(w, h, d, dw, null), seed);
            SpawnPlayer();
        }
        private static void SpawnPlayer()
        {
            if (player != null)
                map.ResetPlayer();

            player = new Player(MathV.Zero);
            map.SetPlayer(player);

            playerHpInfo = new Text("100/100", JBM, 40) { FillColor = new Color(9, 245, 5), OutlineThickness = 1 };
            FloatRect fr = playerHpInfo.GetLocalBounds();

            playerHpInfo.Position = new Vector2f(w - fr.Width - 10, 10);
        }
        public static void NoPlayer()
        {
            player = null;
            SetEndgameText("Game over!", new Color(245, 72, 66));
            
        }
        public static void Win()
        {
            SetEndgameText("You win!", new Color(5, 245, 161));
        }
        private static void SetEndgameText(string text, Color color)
        {
            Text t = new Text(text, JBM, 100) { FillColor = color, OutlineThickness = 1 };
            FloatRect textRect = t.GetLocalBounds();
            t.Position = new Vector2f(w / 2 - textRect.Width / 2, h / 2 - textRect.Height / 2);

            overlays.Add(t);
        }
        public static void DoorTriggered(int door)
        {
            map.Move(door);
        }
        private static void SpawnEnemy(Vector2f pos)
        {
            Random r = new Random();
            float size = (float)r.NextDouble() + .5f;
            Enemy e = new Enemy(pos, size);
            e.Aim = player;
            RegisterObject(e);
        }
        private static void KillEnemies()
        {
            foreach (PhysicsObject c in room.Objects)
                if (c is Enemy)
                    MarkForDelete(c as Character);
        }
        #endregion


        #region debug
        static void ShowDebugInfo()
        {
            string debugLog = string.Format("{0} | ", fps);
            if (player != null)
                debugLog += string.Format("{0}, {1}\n", 
                                      Convert.ToString((int)player.Position.X).PadLeft(4), 
                                      Convert.ToString((int)player.Position.Y).PadLeft(4));
            debugLog += Physics.GetDebugInfo();
            debugLog += map.GetDebugInfo();
            Text text = new Text(debugLog, JBM, 20) { Position = new Vector2f(5, 5), 
                FillColor = new Color(245, 176, 66), OutlineThickness = 1 };

            window.Draw(text);
        }
        #endregion

        #region Other
        static void DrawOverlays()
        {
            foreach (SpriteObject o in overlayObjects)
                o.Draw(window);

            foreach (Drawable d in overlays)
                window.Draw(d);

            window.Draw(playerHpInfo);
        }    
        #endregion
    }
}
