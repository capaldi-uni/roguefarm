﻿using System;
using SFML.System;
using SFML.Graphics;

namespace Roguefarm
{
    class SpriteObject : PhysicsObject
    {
        public class HitboxParam
        {
            public float rad;
            public Color color;
            public float thickness;
        }

        private CircleShape _hitbox;
        private Vector2f _prev_hit = MathV.Zero;
        private Vector2f _prev_sprite = MathV.Zero;
        private float _thickness;

        public Sprite Sprite { get; set; } = null;

        public SpriteObject(Vector2f position, float mass, HitboxParam param) : base(position, param.rad, mass)
        {
            _hitbox = new CircleShape(param.rad - param.thickness)
            {
                FillColor = param.color,
                OutlineColor = new Color(0, 0, 0),
                OutlineThickness = param.thickness
            };

            _thickness = param.thickness;
            Position = position;
        }

        public void Draw(RenderWindow w)
        {
            if (Sprite == null)
                return;

            //Flip sprite depending on velocity direction
            if (_velocity.X > 0)
                Sprite.Scale = new Vector2f(MathF.Abs(Sprite.Scale.X), Sprite.Scale.Y);
            else if (_velocity.X < 0)
                Sprite.Scale = new Vector2f(MathF.Abs(Sprite.Scale.X) * -1, Sprite.Scale.Y);

            if (_center != _prev_sprite)    //Move sprite if objects' postion changed
            {
                _prev_sprite = _center;
                FloatRect fr = Sprite.GetLocalBounds();
                Sprite.Position = _center - new Vector2f(fr.Width / 2 * Sprite.Scale.X, fr.Height / 2 * Sprite.Scale.Y);
            }

            w.Draw(Sprite);
        }

        //Hitbox display for debug
        public void DrawHitbox(RenderWindow w)
        {
            if (_center != _prev_hit)
            {
                _prev_hit = _center;
                _hitbox.Position = _center - new Vector2f(_radius - _thickness, _radius - _thickness);
            }
            w.Draw(_hitbox);
        }
    }
}
